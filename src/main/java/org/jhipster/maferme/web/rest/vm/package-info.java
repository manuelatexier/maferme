/**
 * View Models used by Spring MVC REST controllers.
 */
package org.jhipster.maferme.web.rest.vm;
